#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  analysis.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@mira>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from sys import stdout
from sources.utils import *
import pickle, random, os, sys, time
import torch as t
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader
from pytorchUtils import pytorchUtils as pyu
from SA import selfatt_RRN as SARNN
from SA import sortForPadding, pad_sequence, sortForPaddingSS
import marshal

class predictDataset(Dataset):
    
	def __init__(self, X, lens):
		self.X = X 
		self.lens = lens
		assert len(self.X) > 0
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.lens[idx]

class myDataset(Dataset):
    
	def __init__(self, X, Y, lens):
		self.Y = Y
		self.X = X 
		self.lens = lens
		assert len(self.X) > 0
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.Y[idx], self.lens[idx]

class NNwrapper():

	def __init__(self, model):
		self.model = model
	
	def fit(self, originalX, originalY, lens, epochs = 100, batch_size=100, save_model_every=10, LOG=False, dev=t.device("cpu")):

		self.model.train()
		if LOG:
			os.system("rm -rf ./logs")
			os.system("killall tensorboard")
			os.system("tensorboard --logdir='./logs' --port=6006 &")
			from pytorchUtils.logger import Logger, to_np, to_var
			logger = Logger('./logs')			
			os.system("firefox http://127.0.0.1:6006  &")
		########DATASET###########
		dataset = myDataset(originalX, originalY, lens)
		
		#######MODEL##############		
		parameters =list(self.model.parameters())
		p = []
		for i in parameters:
			p+= list(i.data.cpu().numpy().flat)
		print ('Number of parameters=',len(p))
		p = None	
		self.model.train()
		print ( "Training mode: ", self.model.training)

		print ("Start training")
		########LOSS FUNCTION######
		#loss_fn1 = t.nn.BCELoss(size_average=False)
		#loss_fn1 = AUCLoss()
		loss_fn1 = t.nn.BCEWithLogitsLoss(reduction="sum")
		########OPTIMIZER##########	
		self.learning_rate = 1e-2	
		optimizer = t.optim.Adam(parameters, lr=self.learning_rate, weight_decay=1e-6)
		scheduler = t.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=2, verbose=True, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08)
		
		########DATALOADER#########
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None, num_workers=0)
		minLoss = 1000000000
		ofp = open("models/"+self.model.name+".TrainLog","w",0)		
		#######TRAINING ITERATIONS#############
		e = 0
		while e < epochs:			
			errTot = 0
			i = 1
			start = time.time()
			optimizer.zero_grad()
			t1 = time.time()	
			for sample in loader:						
				x, y1, l = sample
				x = x.to(dev)
				y1 = y1.to(dev)
				l = l.to(dev)
				#print "new sample: ", x.size()
				#print y.size()
				yp = self.model.forward(x, l)
				#print yp.size(), y1.size()
				loss1 = loss_fn1(yp, y1)	
				errTot += loss1.data					
				loss1.backward()
				#print i	
				optimizer.step()
				optimizer.zero_grad()
				t2 = time.time()
				if i % 10 == 0:
					sys.stdout.write("\rbatch: %d/%d (%3.2f%%) %fs" % (i, len(dataset), 100* (i/float(len(dataset))), t2-t1))
				i += batch_size								
			end = time.time()						
			print (" epoch %d, ERRORTOT: %f (%fs)" % (e,errTot, end-start))
			if LOG:
				loss = errTot
				net = self.model
				step = e
				#============ TensorBoard logging ============#
				# (1) Log the scalar values
				info = {'loss': loss}

				for tag, value in info.items():
					logger.scalar_summary(tag, value, step+1)

				# (2) Log values and gradients of the parameters (histogram)
				for tag, value in net.named_parameters():
					tag = tag.replace('.', '/')
					logger.histo_summary(tag, to_np(value), step+1)
					logger.histo_summary(tag+'/grad', to_np(value.grad), step+1)
				#============ TensorBoard logging ============#
			scheduler.step(float(errTot))
			if e % save_model_every == 0 and e > 0:
				print ("Store model ", e)
				t.save(self.model, "models/"+self.model.name+".iter_"+str(e)+".t")				
			stdout.flush()
										
			e += 1	
		t.save(self.model, "models/"+self.model.name+".final.t")
		ofp.close()		
	
	def predict(self, X, lens, batch_size = 701, dev=t.device("cpu"), plotGraph=False, ATT_INFO = False):
		self.model.eval()
		print( "Training mode: ", self.model.training)
		if plotGraph:
			from pytorchUtils.torchgraphviz1 import make_dot, make_dot_from_trace
		print ("Predicting...")
		dataset = predictDataset(X, lens)
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None, num_workers=2)
		yp = []	
		att = []
		out = []
		first = True
		for sample in loader:
			x, l = sample	
			x = x.to(dev)
			l = l.to(dev)
			if ATT_INFO:
				y, a, o = self.model.forward(x, l, ATT_INFO)
			else:
				y = self.model.forward(x, l)
			pred = t.sigmoid(y)
			#pred = self.model.forward(x, xss, l)
			if first and plotGraph:
				first = False
				print( "printing")
				#print dict(self.model.named_parameters())
				#raw_input()
				make_dot(pred.mean(), params=dict(self.model.named_parameters()))		
			if ATT_INFO:
				att += a.tolist()
				out += o.tolist()
			yp += pred.tolist()
		if ATT_INFO:
			return yp, att, out
		else:
			return yp

def main(args):
	if len(args) <= 1:
		print ("USAGE: python %s DEVICE" % args[0])
	DEVICE = args[1]
	print ("Reading trainset...")
	#db = readDatasets("datasets/TargetTrack_sol", "datasets/TargetTrack_insol") #db = {ID : (seq, label) }
	db = readPARSNIPdataset("datasets/PaRSnIP-master/Training_set.txt")
	trainSet = db.keys()#[:1000]
	#random.shuffle(trainSet)
	trainSet = trainSet[:]

	X, Y, lens, corresp = buildVectors(trainSet, db)

	print( "SizeX ", X.size())
	print( "SizeY ", Y.size())
	TRAIN = False 
	#TRAIN = False
	if TRAIN:
		#model = t.load("models/secretionSimpleCnnEmb+DrpOut+SELU.iter_200.t")
		#model = soulNN("soulNNsimple.rawidata")
		model = SARNN(20, hidden_size = 20, num_layers = 2, heads = 20)
		model.to(t.device(DEVICE))
		wrapper = NNwrapper(model)
		wrapper.fit(X, Y, lens, epochs = 50, batch_size=1001, dev = t.device(DEVICE))	
	else:
		model = t.load("models/SA1.82aucauprc.t", map_location="cpu").to(DEVICE)
		wrapper = NNwrapper(model)
		
	print( "Predict trainset...")
	Yp = wrapper.predict(X, lens, dev=t.device(DEVICE))
	print( len(Yp), len(Y))
	getScoresSVR(Yp, Y, threshold=None, PRINT = True, CURVES = False, SAVEFIG=None)
	del db, trainSet, lens
	
	print( "Predict test set...")
	#db = readDatasets("datasets/Ecoli_sol", "datasets/Ecoli_insol") #db = {ID : (seq, label) }
	#db = readPARSNIPdataset("datasets/PaRSnIP-master/Test_set.txt")
	db = readPARSNIPdataset("atpDataset.txt")
	testSet = db.keys()	
	x, y, lensPred, corresp = buildVectors(testSet, db)
	print ("Built %d vectors" % len(x))
	assert len(x) == len(y)	
	yp, a, o = wrapper.predict(x, lensPred, dev=t.device(DEVICE), ATT_INFO = True)
	tmpAtt = []
	tmpO = []
	i = 0
	print ( len(a), len(o), len(lensPred), len(corresp))
	assert len(a) == len(o) == len(lensPred)
	while i < len(a):
#print a[i]
#print lensPreds[i]
		tmpAtt.append(a[i][:lensPred[i]])
		tmpO.append(o[i][:lensPred[i]])
		i+=1
	cPickle.dump((corresp, yp, tmpAtt, tmpO), open("NoEnds_corr_pred_att_out.cPickle", "w"))
	getScoresSVR(yp, y, threshold=None, PRINT = True, CURVES = False, SAVEFIG=None)
	print ("Through Eden took thir solitarie way.")

def buildVectors(uids, db):	#db = {uid:(seq,label)}
	X = []
	Y = []
	corresp = []
	for u in uids:
		tmp = db[u][0]
		#tmp = db[u][0][int(len(db[u][0])*0.21):-int(len(db[u][0])*0.21)]
		#if len(tmp) < 100:
		#	continue
		X.append(getEmbeddingValues(tmp))
		#X.append(seq2vec(tmp))
		Y.append(db[u][1])
		corresp.append(u)
	X, Y, order = sortForPadding(X, Y)
	X, lens = pad_sequence(X, batch_first=True)
	tmpCorresp = []
	for i in order:
		tmpCorresp.append(corresp[i])
	return X, Y, lens, tmpCorresp

def getEmbeddingValues(s):
	r = []
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	for i in s:
		if i not in listAA:
			r.append(0)
		else:
			r.append(listAA.index(i)+1)	
	return r

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
