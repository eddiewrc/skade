#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2014 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
import numpy as np
verbosity = 1
scores = {"good":0, "promising":1, "poor":2, "unfolded":3}

def readFASTA1(seqFile, MIN_LENGTH = 20, MAX_LENGTH=500):
	ifp = open(seqFile, "r")
	sl = []
	i = 0
	line = ifp.readline()
	while len(line) != 0:
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx	
			sl.append([line.strip().replace("|","-").replace(">","")[:14],""]) #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				sl[i][1] = sl[i][1] + line.strip()
				line = ifp.readline()
			i = i + 1
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	db = {}
	discarded = 0
	for p in sl:
		if len(p[1]) < MIN_LENGTH or len(p[1]) > MAX_LENGTH:
			discarded += 1
			continue
		else:
			if  db.has_key(p[0]):
				print( p[0])
			db[p[0]] = p[1]
	print( "Found %d sequences, added %d discarded %d" % (len(sl), len(db), discarded))
	return db

def readFASTA(seqFile, MIN_LENGTH = 20, MAX_LENGTH=500):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line.strip().replace("|","-").replace(">",""),""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = tmp[1]			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print( "Found %d sequences, added %d discarded %d" % (i, len(sl), discarded))
	return sl

def readHSQC(listFiles):
	db = {}
	res = [0,0,0,0] #good, promising, poor, unfolded
	print( listFiles)
	
	for f in listFiles:
		ifp = open(f)
		lines = ifp.readlines()
		ifp.close()
		lines.pop(0)		

		for l in lines:
			tmp = l.strip().split("\t")			
			if len(tmp[3]) > 0 and (tmp[3] == "good" or tmp[3] == "promising" or tmp[3] == "poor" or tmp[3] == "unfolded"):					
				db[tmp[0]] = [tmp[3].upper(), tmp[4].upper()]
				res[scores[tmp[3]]]+=1	
				
	print( "Found %d sequences" % len(db.keys()))
	print( "good, promising, poor, unfolded ", res)
	
	return db# db = {name:[score, seq]}
	
def retrieveUID(l):
	uidList = []
	for i in l:
		ifp = open(i, "r")
		lines = ifp.readlines()
		for j in lines:
			uidList.append(j.strip())		
	if verbosity >= 2:
		print ("Num of uid retrieved: " + str(len(uidList)))
	return uidList	
	
def getSeqIDsimilarity(s1,s2): #change it with your favourite similarity measure!
	score = 0
	i = 0 
	while i < min(len(s1), len(s2)):
		if s1[i] == s2[i] and s1[i] != "-":
			score += 1
		i += 1
	return score/float(len(s2)-s2.count("-")) 
	#return score/float(len(s1))
	#return score/float(len(s1))		
	

def readPARSNIPdataset(f):
	db = {}
	count = [0,0]#sol, insol
	ifp = open(f)
	lines = ifp.readlines()	
	ifp.close()
	for i, l in enumerate(lines):
		tmp = l.strip().split(" ")
		assert len(tmp) == 2
		seq = tmp[0]
		label = int(tmp[1])
		db[i] = (seq, label)
		count[label] += 1
	print ("Unsol, sol: " +str(count))
	print ("Tot: " + str(sum(count)))
	return db

def readDatasets(sol, insol):
	db = {}	
	
	count = [0,0]#sol, insol
	ifp = open(sol)
	lines = ifp.readlines()	
	ifp.close()
	uid = 0
	for l in lines:
		tmp = l.strip().split(" ")
		if len(tmp) == 1:
			name = uid
			seq = tmp[0].upper()
			uid += 1
		else:
			name = tmp[0]
			seq = tmp[1].upper()
		db[name] = (seq,1)
		count[1] += 1
	
	ifp = open(insol)
	lines = ifp.readlines()	
	ifp.close()
	for l in lines:
		tmp = l.strip().split(" ")
		if len(tmp) == 1:
			name = uid
			uid += 1
			seq = tmp[0].upper()
		else:
			name = tmp[0]	
			seq = tmp[1].upper()	
		db[name] = (seq,0)
		count[0] += 1
		
	print (count)
	return db
	
def writeFasta(db, name):
	ofp = open(name, "w")
	for i in db.items():
		ofp.write(">"+str(i[0])+"\n"+i[1][0]+"\n")
	ofp.close()

def main():
	
	db = readFASTA1("../universe/uni20.fasta", MIN_LENGTH = 20, MAX_LENGTH=500)
	db = readFASTA("../universe/uni20.fasta", MIN_LENGTH = 20, MAX_LENGTH=500)
	#db = readDynaFormat("../HSQC/dynaMineResults/toronto_backbone.pred")
	#print len(db)
	#raw_input()
	
	#db = readHSQC(["../HSQC/torontoData/good_20aug2014.txt","../HSQC/torontoData/poor1_20aug2014.txt","../HSQC/torontoData/poor2_20aug2014.txt","../HSQC/torontoData/promising_20aug2014.txt"])
	#for i in db.items():
	#	print ">"+i[0]+"\n"+i[1][1]
	#return
	#db = readDatasets("../datasets/TargetTrack_sol", "../datasets/TargetTrack_insol")
	#writeFasta(db, "TargetTrack.fasta")
	#db = readDatasets("../datasets/Ecoli_sol", "../datasets/Ecoli_insol")
	#writeFasta(db, "Ecoli.fasta")
	#db = readDatasets("../datasets/SOLpro_sol", "../datasets/SOLpro_insol")
	#writeFasta(db, "SOLpro.fasta")
	#db = readDatasets("../datasets/PROSOII_sol", "../datasets/PROSOII_insol")
	#writeFasta(db, "PROSOII.fasta")
	

if __name__ == '__main__':
	main()


