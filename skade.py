#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  skade.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@mira>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy as np
from sys import stdout
import pickle, random, os, sys, time, marshal, copy, gc
import torch as t
import sources.utils as U
from sources.soulValid_attention import buildVectors, NNwrapper, readPARSNIPdataset
from SA import selfatt_RRN as SARNN

def readFASTAforSKADE(seqFile, MIN_LENGTH = 20, MAX_LENGTH=5000):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line.strip().replace("|","-").replace(">",""),""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = (tmp[1], 0)			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print("Found %d sequences, added %d discarded %d" % (i, len(sl), discarded))
	return sl

def main(args):
	if (len(args) != 3 and len(args) != 2) or "-h" in args[1]:
		print( "\nUSAGE: python skade.py INPUT_FASTA [DEVICE]\n")
		print("\tINPUT_FASTA is a fasta file containing the target sequences")
		print( "\tDEVICE is an optional argument (default: cpu) that permits to specify the device on which SKADE should be run. Possible options are cpu, cuda (e.g. cuda:1 if there are many GPUs)\n")
		return 1
	print( "SKADE 1.0")
	print( "Target file: %s" % args[1])
	db = readFASTAforSKADE(args[1])
	if len(args) > 2:
		DEVICE = args[2]
	else:
		DEVICE = "cpu"
	print( "Chosen device: %s " % DEVICE)
	testSet = db.keys()	
	model = SARNN(20, hidden_size = 20, num_layers = 2, heads = 20).to(DEVICE)
	model.load_state_dict(t.load(open("models/skadeParams.t", "rb")))
	#model = t.load("models/SA1.82aucauprc.t", map_location='cpu').to(DEVICE)
	#t.save(model.state_dict(), open("skadeParams.t","w"))
	wrapper = NNwrapper(model)
	x, y, lensPred, corresp = buildVectors(db.keys(), db)
	print ("Built %d vectors" % len(x))
	assert len(x) == len(y)	
	yp, a, o = wrapper.predict(x, lensPred, dev=t.device(DEVICE), ATT_INFO = True)
	tmpAtt = []
	tmpO = []
	i = 0
	print( len(a), len(o), len(lensPred), len(corresp))
	assert len(a) == len(o) == len(lensPred)
	while i < len(a):
		tmpAtt.append(a[i][:lensPred[i]])
		tmpO.append(o[i][:lensPred[i]])
		i+=1
	a = tmpAtt
	o = tmpO
	print ("Prediction successfully computed.")
	regPred = {}
	ofp = open(args[1]+".skadePredictions.tsv", "w")
	ofp1 = open(args[1]+".skadeAttention.tsv", "w")
	ofp2 = open(args[1]+".skadeSolubilityProfile.tsv", "w")
	ofp.write("Protein_ID\tSKADE_PREDICTION\n")
	for c, prot in enumerate(corresp):
		ofp.write("%s\t%f\n"% (prot, yp[c]))
		regPred[prot] = []
		assert len(a[c]) == len(o[c])
		i = 0
		while i < len(a[c]):
			regPred[prot].append(a[c][i]*o[c][i])
			i+=1
		ofp1.write("%s\t%s\n"% (prot, toTsv(a[c])))
		ofp2.write("%s\t%s\n"% (prot, toTsv(regPred[prot])))
	ofp.close()
	ofp1.close()
	ofp2.close()
	print ("Predictions stored in "+args[1]+".skadePredictions.tsv")
	print ("Attention values stored in "+args[1]+".skadeAttention.tsv")
	print ("Solubility profiles (pred[i] * att[i]) stored in "+args[1]+".skadeSolubilityProfile.tsv")
	
	print ("Finished.\n")
	
	print ("\"All machines knew what would happen to them when their masters lost faith in their infallibility.\" \n(Absolution Gap, A. Reynolds)")

def toTsv(l):
	s = ""
	for i in l:
		s+=str(i)+"\t"
	return s
	

def predictions2seqs(yp, corresp, ypwt, lenSeq):
	finalseq = np.zeros((lenSeq, 20))
	i = 0
	assert len(yp) == len(corresp)
	while i < len(corresp):
		tmp = corresp[i].split(":")
		pos = int(tmp[1])
		aaPos = listAA.index(tmp[2])
		finalseq[pos][aaPos] = yp[i] - ypwt
		i+=1
	return finalseq

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
