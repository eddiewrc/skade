# WHAT IS SKADE #

SKADE is a Neural Network based method for the prediction of protein solubility

More derails will be soon available in:
`Raimondi et al., ...`

### What is this repository for? ###

The code here contains a standalone version of SKADE (`skade.py`), which takes as input protein sequences in FASTA format and outputs the predicted solubility for each of them. 
An examples of a suitable FASTA file is available in `EXAMPLE/example.fasta`.

The original data in the example files are the proteins of the Amyl33 dataset, also available from: `https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0054175` .

### DATA AVAILABILITY ###

All the datasets mentioned in the paper are publicly available from the respective papers and repositories.

### How do I set it up? ###

SKADE has some dependencies, which are popular pytohn libraries (such as pytorch, numpy and scipy). 
Here we show how to create a miniconda environment containig all those libraries. Similar instructions can be used to install the dependencies with `pip`. 
SKADE runs on python 2.7 .

* Download and install miniconda from `https://docs.conda.io/en/latest/miniconda.html`
* Create a new conda environment by typing: `conda create -n SKADE -python=2.7`
* Enter the environment by typing: `conda activate SKADE`
* Install pytorch >= 1.0 with the command: `conda install pytorch -c pytorch`
* Install numpy with the command: `conda install numpy`
	
You can remove this environment at any time by typing: conda remove -n SKADE --all
 

### What is this repository contains? ###

* `SKADE.py` -> is the standalone predictor.
* `SA.py` -> Contains the attention modules necessary to run SKADE.
* `models/` -> contains the serialized version of the trained model, necessary to run SKADE
* `sources/` -> folder containing source code necessary to run SKADE
* `pytorchUtils/` -> folder containing source code necessary to run SKADE
* `EXAMPLE/` -> folder containing a runnable exmple of FASTA file that could be used as input for SKADE
* `EXAMPLE/marshalled` -> example fasta file
* `EXAMPLE/example.fasta.skade*` -> Tab Separated Values files containig precomputed SKADE predictions
* `README.md` -> this readme

### How do I predict proteins with SKADE? ###


You can run one of our examples by typing:

```
(SKADE) eddiewrc@alnilam:~/Bioinformatics/projects/skade$ python skade.py EXAMPLE/example.fasta
```

You will obtain the following results:

```
SKADE 1.0
Target file: EXAMPLE/example.fasta
Found 34 sequences, added 34 discarded 0
Chosen device: cpu 
/home/eddiewrc/miniconda2/envs/pytorch1/lib/python2.7/site-packages/torch/serialization.py:454: SourceChangeWarning: source code of class 'SA.selfatt_RRN' has changed. you can retrieve the original source code by accessing the object's source attribute or set `torch.nn.Module.dump_patches = True` and use the patch tool to revert the changes.
  warnings.warn(msg, SourceChangeWarning)
Built 34 vectors
Training mode:  False
Predicting...
34 34 34 34
Prediction successfully computed.
Predictions stored in EXAMPLE/example.fasta.skadePredictions.tsv
Attention values stored in EXAMPLE/example.fasta.skadeAttention.tsv
Solubility profiles (pred[i] * att[i]) stored in EXAMPLE/example.fasta.skadeSolubilityProfile.tsv
Finished.

"All machines knew what would happen to them when their masters lost faith in their infallibility." 
(Absolution Gap, A. Reynolds)
```

The final prediction are stored in the EXAMPLE/example.fasta.skade* files.



### Who do I talk to? ###

Please report bugs at the following mail addresses:

daniele DoT raimondi aT kuleuven DoTbe

yves dOt moreau At kuleuven dOt be
