#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pytorchUtils.py
#  
#  Copyright 2018 Daniele Raimondi <daniele.raimondi@vub.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import torch as t
from torch.autograd import Variable
from torch.nn.utils.rnn import PackedSequence, pack_padded_sequence, pad_packed_sequence

def last_timestep(self, unpacked, lengths):
	# Index of the last output for each sequence.
	idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
	return unpacked.gather(1, idx).squeeze()

def setNumCores(v=-1):
	if v != -1:
		import multiprocessing
		v = multiprocessing.cpu_count()
	t.set_num_threads(v)
	print("> Set to use %d cores" % v)

def pack_sequence(sequences,batch_first=True):
	return t.nn.utils.rnn.pack_padded_sequence(pad_sequence(sequences,batch_first=batch_first), [v.size(0) for v in sequences],batch_first=batch_first)

def pad_sequence(sequences, batch_first=True):
	max_size = sequences[0].size()
	max_len, trailing_dims = max_size[0], max_size[1:]
	prev_l = max_len
	if batch_first:
		out_dims = (len(sequences), max_len) + trailing_dims
	else:
		out_dims = (max_len, len(sequences)) + trailing_dims
	out_variable = sequences[0].new(*out_dims).zero_()
	lengths=[]
	for i, variable in enumerate(sequences):
		length = len(sequences[i])
		lengths.append(length)# temporary sort check, can be removed when we handle sorting internally
		if prev_l < length:
			raise ValueError("lengths array has to be sorted in decreasing order")
		prev_l = length
		# use index notation to prevent duplicate references to the variable
		if batch_first:
			out_variable[i, :length, ...] = variable
		else:
			out_variable[:length, i, ...] = variable
	return out_variable, lengths

def pad_VariableList(sequences, dsMax = None, batch_first=True):
	#print len(sequences)
	max_size = sequences[0].size()
	if dsMax != None:
		max_len = dsMax
	else:
		max_len = max_size[0]
	trailing_dims = [int(l) for l in list(max_size[1:])]
	#print trailing_dims
	lengths = []
	seqs = []
	for i, var in enumerate(sequences):
		length = len(sequences[i])
		lengths.append(length)
		if length == max_len:
			seqs.append(var)
			continue		
		missingDims =  int(max_len-var.size()[0])
		tmp = t.cat([var,Variable(t.zeros([missingDims]+ trailing_dims))])
		#print tmp.size()
		assert tmp.size()[0] == max_len
		seqs.append(tmp)
		#print tmp.size()
	seqs = t.stack(seqs,0)
	assert len(seqs) == len(lengths)
	return seqs, lengths

def sortForPadding(X, Y1):
	assert len(X) == len(Y1)
	tmp = []
	i = 0
	while i < len(X):
		tmp.append((X[i], Y1[i], i))
		i+=1
	tmp = sorted(tmp, key=lambda x: len(x[0]), reverse=True)
	i = 0
	X = []
	Y1 = []
	Y2 = []
	order = []
	while i < len(tmp):
		X.append(t.Tensor(tmp[i][0]))
		Y1.append(t.Tensor(tmp[i][1]))		
		order.append(tmp[i][-1])
		i+=1
	return X, Y1, order #lists of tensors

def unpad(x, l):
	preds=[]
	for i in xrange(len(x)):
		preds.append(x[i][:l[i]])
	return preds
	
def reconstruct(x, l):
	preds=[]
	pos = 0
	#print x.size()
	for p in l:
		tmp = x[pos:pos+p]
		#print tmp.size()
		preds.append(tmp)
		pos = p
	return preds	
